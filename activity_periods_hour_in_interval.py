import pandas as pd
import numpy as np


def activity_periods_of_the_day(path_to_data_frame, date_column,
                            transaction_column, # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                            column_for_analysis_name,
                            number_of_hours_in_interval,
                            output_file = None,
                            is_normalized = None,
                            product_column='StockCode',
                            user_column='CustomerID',
                            quantity_column='Quantity',
                            list_of_excluded_hours = []):
                                                                    # excluded_hours is a list of hours (from 0 to 23) which are excluded from the consideration
                                                                    # product_or_user == 'user' counts visits; product_or_user == 'product' counts quantity
    df = pd.read_csv(path_to_data_frame)
    # convert data string to hours (int)
    df[date_column] = pd.to_datetime(df[date_column])
    df[date_column] = df[date_column].dt.hour

    #if there is no a transaction column in the data_frame then we create it and assign an index value to the transaction column
    if transaction_column == 'index':
        df[transaction_column] = df.index

    # exclusion of hours, chosen in list_of_excluded_hours
    df_selected = df.loc[~(df[date_column].isin(list_of_excluded_hours))].copy()

    # create an array of considered hours
    list_of_considered_hours = df_selected[date_column].unique()
    list_of_considered_hours.sort()
    # append an extra element to the end of array in order not to have problems with names of columns
    list_of_considered_hours = np.append(list_of_considered_hours, list_of_considered_hours[len(list_of_considered_hours) - 1] + 1)

    # creation of 'Time Interval' column and full filling it by the strings what correspond to the interval what is suitable for a considered product_or_user
    min_hour_value_in_interval = 0
    while min_hour_value_in_interval < len(list_of_considered_hours) - 1:
           # we use min() function in order to prevent the situation when index will exceed an array's length
            df_selected.loc[(df_selected[date_column] >= list_of_considered_hours[
                min_hour_value_in_interval]) & (df_selected[date_column] <= list_of_considered_hours[
                 min(min_hour_value_in_interval +  number_of_hours_in_interval - 1, len(list_of_considered_hours) - 1)]), 'Time Interval'] = \
                        '{}-{}'.format(list_of_considered_hours[min_hour_value_in_interval],
                            list_of_considered_hours[min(min_hour_value_in_interval + number_of_hours_in_interval, len(list_of_considered_hours) - 1)])
            #print(df_selected['Time Interval'].unique())
            min_hour_value_in_interval+= number_of_hours_in_interval
    sorted_columns = df_selected['Time Interval'].unique()


   # creation of a df, where indexes are product_or_user and colums are time intervals
    if column_for_analysis_name == product_column:
        df_grouped_by_column_for_analysis_time_interval = df_selected.groupby([column_for_analysis_name, 'Time Interval'])[quantity_column].sum().unstack().fillna(0)
    elif column_for_analysis_name == user_column:
        # here  [quantity_column].count() is just a dummy; without it we can not use a grouped df on the next step.
        # on my home computer [transaction_column].count() made an error "cannot insert Transaction ID, already exists"
        df_grouped_by_column_for_analysis_time_interval_transaction = df_selected.groupby([column_for_analysis_name, transaction_column, 'Time Interval']
                                                                                          ,as_index=False)[quantity_column].count()
        #counting the number of visits for each user in a considered time interval
        df_grouped_by_column_for_analysis_time_interval = df_grouped_by_column_for_analysis_time_interval_transaction.groupby(
                                                                [column_for_analysis_name, 'Time Interval'])[transaction_column].count().unstack().fillna(0)
    else:
        print('column_for_analysis_name is not correct')

    df_grouped_by_column_for_analysis_time_interval = df_grouped_by_column_for_analysis_time_interval[sorted_columns]
    df_grouped_by_column_for_analysis_time_interval['Total'] = df_grouped_by_column_for_analysis_time_interval.sum(axis=1)

    # normalization: create a list of intervals (columns of a df_grouped_by_column_for_analysis_time_interval)
     # and iterate over them, dividing on a total amount of visits/quantities

    if is_normalized is not None:
        for interval in sorted_columns:
            df_grouped_by_column_for_analysis_time_interval[interval] = \
                df_grouped_by_column_for_analysis_time_interval[interval]/df_grouped_by_column_for_analysis_time_interval['Total']

    if output_file is not None:

        return df_grouped_by_column_for_analysis_time_interval.sort_values('Total', ascending=False).reset_index().to_csv(
                            '{}.csv'.format(output_file), index=False)
    else:
        return df_grouped_by_column_for_analysis_time_interval.sort_values('Total', ascending=False).reset_index()


periods = activity_periods_of_the_day('kauia_dataset_excluded_extras.csv',
                            'Transaction Date',
                            'Transaction ID', # if there is no a transaction column in the data_frame then put transaction_column = 'index'
                            'Member ID',
                            number_of_hours_in_interval = 1,
                            output_file = 'Member ID_hours_in_interval_1',
                            product_column='Product Name',
                            user_column='Member ID',
                            quantity_column='Product Quantity',
                            list_of_excluded_hours = [0,1,18,19,20,21,22,23])
